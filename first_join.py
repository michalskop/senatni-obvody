"""Join okrsky with obvody, except PBOP."""

import csv
import datetime
import io
import requests

path = "/home/michal/dev/senatni-obvody/"

d = datetime.datetime.today().strftime('%d.%m.%Y')
url1 = "http://apl.czso.cz/iSMS/cisexp.jsp?kodcis=43&typdat=1&cisvaz=120_1686&datpohl=" + d + "&cisjaz=203&format=2&separator=%2C"

r = requests.get(url1)
r.encoding = 'cp1250'
csvio = io.StringIO(r.text, newline="")
data = []
for row in csv.DictReader(csvio):
    data.append(row)

data[0]

url2 = "http://apl.czso.cz/iSMS/cisexp.jsp?kodcis=43&typdat=1&cisvaz=1057_304&datpohl=" + d + "&cisjaz=203&format=2&separator=%2C"

r = requests.get(url2)
r.encoding = 'cp1250'
csvio = io.StringIO(r.text, newline="")
data2 = []
data2_obj = {}
for row in csv.DictReader(csvio):
    data2.append(row)
    data2_obj[row['CHODNOTA1']] = row

data2[0]

# Praha, Brno, Ostrava, Plzeň
pbop = ['554782', '582786', '554821', '554791']

not_ins = []

header = ['municipality_id', 'municipality', 'polling_station_id', 'polling_station', 'senate_district_id', 'senate_district']

with open(path + "list_raw.csv", "w") as fout:
    dw = csv.DictWriter(fout, header)
    dw.writeheader()
    for row in data:
        try:
            item = {
                'municipality_id': row['CHODNOTA1'],
                'municipality': row['TEXT1'],
                'polling_station_id': row['CHODNOTA2'],
                'polling_station': row['TEXT2'],
                'senate_district_id': data2_obj[row['CHODNOTA1']]['CHODNOTA2'],
                'senate_district': data2_obj[row['CHODNOTA1']]['TEXT2']
            }
            if row['CHODNOTA1'] in pbop:
                item['senate_district_id'] = ''
                item['senate_district'] = ''
            dw.writerow(item)

        except Exception:
            if row['CHODNOTA1'] not in not_ins:
                not_ins.append(row['CHODNOTA1'])
            print(row['CHODNOTA1'], row['TEXT1'])
