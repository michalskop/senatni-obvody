"""Finds errors in polling stations between RUIAN and CZSO.cz and tries to repaire thems."""

import csv
# import io
# import requests
import shapefile

path = "/home/michal/dev/senatni-obvody/"
data_path = "ruian/"
output_path = "geo/"

# list of municipalities
municipalities = []
municipalities_obj = {}
with open(path + "municipalities.csv") as fin:
    dr = csv.DictReader(fin)
    for row in dr:
        municipalities.append(row)
        municipalities_obj[row['municipality_id']] = row

# list of senate districts
senate_districts_obj = {}
with open(path + "senate_districts.csv") as fin:
    dr = csv.DictReader(fin)
    for row in dr:
        senate_districts_obj[row['senate_district_id']] = row

# list of polling stations
polling_stations_obj = {}
ps_by_muni_obj = {}
with open(path + "districts_joined.csv") as fin:
    dr = csv.DictReader(fin)
    for row in dr:
        polling_stations_obj[row['polling_station_id']] = row
        ps_by_muni_obj[row['municipality_id']] = row

# list of MOMCs
momcs_obj = {}
with open(path + "momcs.csv") as fin:
    dr = csv.DictReader(fin)
    for row in dr:
        momcs_obj[row['momc_id']] = row

# list of polling stations - manually correcting errors
# polling_stations_manually_obj = {}
# with open(path + "polling_stations_manually.csv") as fin:
#     dr = csv.DictReader(fin)
#     for row in dr:
#         polling_stations_manually_obj[row['station_id']] = row

# prepare columns
cols = ['station', 'muni', 'momc', 'pou', 'orp', 'distr', 'lau1', 'region', 'nuts3', 'type', 'senate']
columns = []
column_names = []
for col in cols:
    columns.append([col + '_id', 'C', 40, 0])
    columns.append([col + '', 'C', 40, 0])
    column_names.append(col + '_id')
    column_names.append(col + '')
columns.append(['senate_cat', 'C', 40, 0])
columns.append(['station_cod', 'C', 40, 0])
column_names.append('senate_cat')
column_names.append('station_cod')

# define shapefile
# w = shapefile.Writer(path + output_path + "cz", shapeType=5)
# w.fields = columns


def _join_rename(item, r):
    item['muni_id'] = r['municipality_id']
    item['muni'] = r['municipality_name']
    item['pou_id'] = r['pou_id']
    item['pou'] = r['pou_name']
    item['orp_id'] = r['orp_id']
    item['distr_id'] = r['district_id']
    item['distr'] = r['district_name']
    item['lau1_id'] = r['lau1_id']
    item['lau1'] = r['lau1_name']
    item['region_id'] = r['region_id']
    item['region'] = r['region_name']
    item['nuts3_id'] = r['nuts3_id']
    item['nuts3'] = r['nuts3_name']
    item['type_id'] = r['type_id']
    item['type'] = r['type_name']
    return item


# created joined shapefile
i = 0
with open(path + "polling_stations_manually_raw.csv", "w") as fout:
    header = ["station_id", "station", "muni_id", "muni", "momc_id", "momc", "pou_id", "pou", "orp_id", "orp", "distr_id", "distr", "lau1_id", "lau1", "region_id", "region", "nuts3_id", "nuts3", "type_id", "type", "senate_id", "senate", "senate_cat", "station_cod"]
    dw = csv.DictWriter(fout, header)
    dw.writeheader()

    for municipality_id in municipalities_obj:
        sf = shapefile.Reader(path + data_path + municipality_id + "/VO_P.shp", encoding="cp1250")
        for shaperec in sf.iterShapeRecords():
            polling_station_id = shaperec.record[0]
            if polling_station_id in polling_stations_obj.keys():
                everything_is_ok = True
            else:
                print(shaperec.record)
                item = {
                    'station_id': polling_station_id,
                    'station': 'Volební místnost ' + str(shaperec.record[1]),
                    'station_cod': str(shaperec.record[1])
                }
                if len(shaperec.record) == 8:
                    row_muni_id = shaperec.record[3]
                    row_momc_id = False
                else:
                    row_muni_id = shaperec.record[2]
                    row_momc_id = shaperec.record[3]
                if not row_momc_id:
                    item = _join_rename(item, municipalities_obj[row_muni_id])
                    item['momc_id'] = ''
                    item['momc'] = ''
                    if row_muni_id in ps_by_muni_obj.keys():
                        item['senate_id'] = ps_by_muni_obj[row_muni_id]['senate_district_id']
                        item['senate'] = ps_by_muni_obj[row_muni_id]['senate_district'].split(': ')[1]
                        category = (int(ps_by_muni_obj[row_muni_id]['senate_district_id']) % 3) * 2
                        if category == 0:
                            category = 6
                        item['senate_cat'] = category
                    else:
                        item['senate_id'] = ''
                        item['senate'] = ''
                        item['senate_cat'] = ''
                else:   # MOMCs
                    item = _join_rename(item, municipalities_obj[row_muni_id])
                    item['momc_id'] = row_momc_id
                    item['momc'] = momcs_obj[row_momc_id]['momc_name']
                    item['senate_id'] = ''
                    item['senate'] = ''
                    item['senate_cat'] = ''
                dw.writerow(item)

        # w.record(*row)
        # w.shape(shaperec.shape)
        # break
        if (i / 1000) == round(i / 1000):
            print(i)
        i += 1

# w.close()

t = 'Volební obvod č. 61, sídlo: Olomouc'
t.split(': ')[1]
sf.fields[1:]
# senate_district_id
polling_station_id
polling_stations_obj[polling_station_id]
shaperec.record
