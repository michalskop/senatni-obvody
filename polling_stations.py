"""Get list of polling stations from volby.cz XMLs."""

import csv
import io
# from lxml import objectify
import xmltodict

path = "/home/michal/dev/senatni-obvody/"

years = [2014, 2016, 2018]

header = ["municipality_momc_id", "municipality_momc", "polling_station_code", "senate_district_id", "year"]
with open(path + "volby_okrsek.csv", "w") as fout:
    dw = csv.DictWriter(fout, header)
    dw.writeheader()
    for year in years:
        # year = 2016
        with io.open(path + 'secoco_' + str(year) + ".xml", encoding="cp1250") as fin:
            text = fin.read()
            obj = xmltodict.parse(text)
            for row in obj['SE_COCO']['SE_COCO_ROW']:
                item = {
                    'municipality_momc_id': row['OBEC'],
                    'municipality_momc': row['NAZEVOBCE'],
                    'senate_district_id': row['OBVOD'],
                    'year': year
                }
                for i in range(1, 10):
                    if int(row['MAXOKRSEK' + str(i)]) > 0:
                        for j in range(int(row['MINOKRSEK' + str(i)]), int(row['MAXOKRSEK' + str(i)]) + 1):
                            item['polling_station_code'] = j
                            dw.writerow(item)
                # break
