"""Prepares list of MOMCs."""

import csv
import datetime
import io
import requests

path = "/home/michal/dev/senatni-obvody/"

# list of momcs districts
d = datetime.datetime.today().strftime('%d.%m.%Y')
url = "http://apl.czso.cz/iSMS/cisexp.jsp?kodcis=44&typdat=1&cisvaz=43_35&datpohl=" + d + "&cisjaz=203&format=2&separator=%2C"
r = requests.get(url)
r.encoding = 'cp1250'
csvio = io.StringIO(r.text, newline="")
sds = []
for row in csv.DictReader(csvio):
    sds.append(row)

header = ['momc_id', 'momc_name', 'municipality_id', 'municipality_name']
with open(path + "momcs.csv", "w") as fout:
    dw = csv.DictWriter(fout, header)
    dw.writeheader()
    for row in sds:
        item = {
            'momc_id': row['CHODNOTA1'],
            'momc_name': row['TEXT1'],
            'municipality_id': row['CHODNOTA2'],
            'municipality_name': row['TEXT2']
        }
        dw.writerow(item)
