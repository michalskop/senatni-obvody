"""Download files from RUIAN."""

import csv
import datetime
import io
import requests
import zipfile

path = "/home/michal/dev/senatni-obvody/"
data_path = "ruian/"

# list of municipalities
d = datetime.datetime.today().strftime('%d.%m.%Y')
url = "http://apl.czso.cz/iSMS/cisexp.jsp?kodcis=43&typdat=0&cisvaz=80007_1771&datpohl=" + d + "&cisjaz=203&format=2&separator=%2C"
r = requests.get(url)
r.encoding = 'cp1250'
csvio = io.StringIO(r.text, newline="")
data = []
for row in csv.DictReader(csvio):
    data.append(row)

# Download
i = 0
errors = []
for row in data:
    # code = "559075"
    code = row['CHODNOTA']

    url = "http://services.cuzk.cz/shp/obec/epsg-5514/" + code + ".zip"

    r = requests.get(url)

    ftypes = ['.dbf', '.prj', '.shp', '.shx']
    fname = 'VO_P'

    extract_path = path + data_path
    if r.ok:
        z = zipfile.ZipFile(io.BytesIO(r.content))
        for ftype in ftypes:
            z.extract(code + '/' + fname + ftype, path=extract_path)
    else:
        errors.append(code)

    i += 1
    if (i % 1000) == 0:
        print(i)

    # z.extractall(path + data_path)
