"""Transform RUIAN data from EPSG 5514 to 4326."""

import geopandas
import os
import shutil

path = "/home/michal/dev/senatni-obvody/"
in_path = "ruian/"
out_path = "ruian_4326/"

subfolders = [f.name for f in os.scandir(path + in_path) if f.is_dir()]

i = 0
for subfolder in subfolders:
    df = geopandas.read_file(path + in_path + subfolder + "/VO_P.shp")
    df = df.to_crs("EPSG:4326")
    os.mkdir(path + out_path + subfolder)
    try:
        df.to_file(path + out_path + subfolder + "/VO_P.shp")
    except Exception:
        shutil.copy(path + in_path + subfolder + "/VO_P.shp", path + out_path + subfolder + "/VO_P.shp")
    # break
    if (i % 100) == 0:
        print(i)
    i += 1

# df.to_file(path + out_path + subfolder + "/VO_P.geojson", driver='GeoJSON')
