"""Get all info about municipalities."""

import csv
import datetime
import io
import requests

path = "/home/michal/dev/senatni-obvody/"

# define all info about municipalities
levels = [
    {
        'name': 'pou',
        'code': '61',
        'code2': '92'
    },
    {
        'name': 'orp',
        'code': '65',
        'code2': '1182'
    },
    {
        'name': 'district',
        'code': '101',
        'code2': '333'
    },
    {
        'name': 'lau1',
        'code': '109',
        'code2': '1770'
    },
    {
        'name': 'region',
        'code': '100',
        'code2': '1250'
    },
    {
        'name': 'nuts3',
        'code': '80038',
        'code2': '1771'
    },
    {
        'name': 'type',
        'code': '67',
        'code2': '32'
    }
]

# download all info about municipalities
data = []
for level in levels:
    ldata = {}
    d = datetime.datetime.today().strftime('%d.%m.%Y')
    url = "http://apl.czso.cz/iSMS/cisexp.jsp?kodcis=43&typdat=1&cisvaz=" + level['code'] + "_" + level['code2'] + "&datpohl=" + d + "&cisjaz=203&format=2&separator=%2C"
    r = requests.get(url)
    r.encoding = 'cp1250'
    csvio = io.StringIO(r.text, newline="")
    for row in csv.DictReader(csvio):
        ldata[row['CHODNOTA1']] = row
    data.append({
        'name': level['name'],
        'level': level,
        'data': ldata
    })

# list of municipalities
url = "http://apl.czso.cz/iSMS/cisexp.jsp?kodcis=43&typdat=0&cisvaz=80007_1771&datpohl=" + d + "&cisjaz=203&format=2&separator=%2C"
r = requests.get(url)
r.encoding = 'cp1250'
csvio = io.StringIO(r.text, newline="")
municipalities = []
for row in csv.DictReader(csvio):
    municipalities.append(row)

# construct output file
header = ['municipality_id', 'municipality_name']
for level in levels:
    header = header + [level['name'] + '_id', level['name'] + '_name']
with open(path + "municipalities.csv", "w") as fout:
    dw = csv.DictWriter(fout, header)
    dw.writeheader()
    for row in municipalities:
        municipality_id = row['CHODNOTA']
        item = {
            'municipality_id': municipality_id,
            'municipality_name': row['TEXT']
        }
        i = 0
        for level in levels:
            attr_id = data[i]['name'] + "_id"
            attr_name = data[i]['name'] + "_name"
            item[attr_id] = data[i]['data'][municipality_id]['CHODNOTA2']
            item[attr_name] = data[i]['data'][municipality_id]['TEXT2']
            i += 1
        dw.writerow(item)
