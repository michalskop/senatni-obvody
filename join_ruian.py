"""Join RUIAN municipalities into one big file and add info about municipalities."""

import csv
# import io
# import requests
import shapefile

path = "/home/michal/dev/senatni-obvody/"
data_path = "ruian_4326/"
output_path = "geo/"

# list of municipalities
municipalities = []
municipalities_obj = {}
with open(path + "municipalities.csv") as fin:
    dr = csv.DictReader(fin)
    for row in dr:
        municipalities.append(row)
        municipalities_obj[row['municipality_id']] = row

# list of senate districts
senate_districts_obj = {}
with open(path + "senate_districts.csv") as fin:
    dr = csv.DictReader(fin)
    for row in dr:
        senate_districts_obj[row['senate_district_id']] = row

# list of polling stations
polling_stations_obj = {}
with open(path + "districts_joined.csv") as fin:
    dr = csv.DictReader(fin)
    for row in dr:
        polling_stations_obj[row['polling_station_id']] = row

# list of polling stations - manually correcting errors
polling_stations_manually_obj = {}
with open(path + "polling_stations_manually.csv") as fin:
    dr = csv.DictReader(fin)
    for row in dr:
        polling_stations_manually_obj[row['station_id']] = row

# prepare columns
cols = ['station', 'muni', 'momc', 'pou', 'orp', 'distr', 'lau1', 'region', 'nuts3', 'type', 'senate']
columns = []
column_names = []
for col in cols:
    columns.append([col + '_id', 'C', 40, 0])
    columns.append([col + '', 'C', 40, 0])
    column_names.append(col + '_id')
    column_names.append(col + '')
columns.append(['senate_cat', 'C', 40, 0])
columns.append(['station_cod', 'C', 40, 0])
column_names.append('senate_cat')
column_names.append('station_cod')

# define shapefile
w = shapefile.Writer(path + output_path + "cz", shapeType=5)
w.fields = columns

# created joined shapefile
i = 0
for municipality_id in municipalities_obj:
    sf = shapefile.Reader(path + data_path + municipality_id + "/VO_P.shp", encoding="cp1250")
    for shaperec in sf.iterShapeRecords():
        polling_station_id = shaperec.record[0]
        if polling_station_id in polling_stations_obj.keys():
            municipality_id = polling_stations_obj[polling_station_id]['municipality_id']
            senate_district_id = polling_stations_obj[polling_station_id]['senate_district_id']
            item = {
                'station_id': polling_station_id,
                'station': polling_stations_obj[polling_station_id]['polling_station'],
                'station_cod': polling_stations_obj[polling_station_id]['polling_station_code'],
                'muni_id': municipality_id,
                'muni': polling_stations_obj[polling_station_id]['municipality'],
                'momc_id': polling_stations_obj[polling_station_id]['momc_id'],
                'momc': polling_stations_obj[polling_station_id]['momc'],
                'pou_id': municipalities_obj[municipality_id]['pou_id'],
                'pou': municipalities_obj[municipality_id]['pou_name'],
                'orp_id': municipalities_obj[municipality_id]['orp_id'],
                'orp': municipalities_obj[municipality_id]['orp_name'],
                'distr_id': municipalities_obj[municipality_id]['district_id'],
                'distr': municipalities_obj[municipality_id]['district_name'],
                'lau1_id': municipalities_obj[municipality_id]['lau1_id'],
                'lau1': municipalities_obj[municipality_id]['lau1_name'],
                'region_id': municipalities_obj[municipality_id]['region_id'],
                'region': municipalities_obj[municipality_id]['region_name'],
                'nuts3_id': municipalities_obj[municipality_id]['nuts3_id'],
                'nuts3': municipalities_obj[municipality_id]['nuts3_name'],
                'type_id': municipalities_obj[municipality_id]['type_id'],
                'type': municipalities_obj[municipality_id]['type_name'],
                'senate_id': senate_district_id,
                'senate': senate_districts_obj[senate_district_id]['senate_district_name'],
                'senate_cat': senate_districts_obj[senate_district_id]['senate_district_category']
            }
            row = []
            for column in column_names:
                row.append(item[column])
        else:
            try:
                item = polling_stations_manually_obj[polling_station_id]
                row = []
                for column in column_names:
                    row.append(item[column])
            except Exception:
                print(shaperec.record)
        w.record(*row)
        w.shape(shaperec.shape)
    # break
    if (i / 1000) == round(i / 1000):
        print(i)
    i += 1

w.close()

# sf.fields[1:]
# senate_district_id
# polling_station_id
# polling_stations_obj[polling_station_id]
# shaperec.record
