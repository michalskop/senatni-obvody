"""Join all 3 files together."""

import csv
import datetime
import io
import requests

path = "/home/michal/dev/senatni-obvody/"

# list of city parts
d = datetime.datetime.today().strftime('%d.%m.%Y')
url = "http://apl.czso.cz/iSMS/cisexp.jsp?kodcis=44&typdat=1&cisvaz=43_35&datpohl=" + d + "&cisjaz=203&format=2&separator=%2C"
r = requests.get(url)
r.encoding = 'cp1250'
csvio = io.StringIO(r.text, newline="")
momc = []
momc_obj = {}
for row in csv.DictReader(csvio):
    momc.append(row)
    if not row['CHODNOTA2'] in momc_obj.keys():
        momc_obj[row['CHODNOTA2']] = {}
    momc_obj[row['CHODNOTA2']][row['CHODNOTA1']] = row

# list of polling stations from volby.cz
volby_obj = {}
with open(path + "volby_okrsek.csv") as fin:
    dr = csv.DictReader(fin)
    for row in dr:
        if not row['municipality_momc_id'] in volby_obj.keys():
            volby_obj[row['municipality_momc_id']] = {}
        volby_obj[row['municipality_momc_id']][row['polling_station_code']] = row

# list of current polling stations
data_obj = {}
with open(path + "list.csv") as fin:
    dr = csv.DictReader(fin)
    for row in dr:
        if not row['municipality_id'] in data_obj.keys():
            data_obj[row['municipality_id']] = {}
        row['polling_station_code'] = row['polling_station'].split(' ')[-1]
        data_obj[row['municipality_id']][row['polling_station_code']] = row

# data_obj['503941']

# add missing senate district ids:
with open(path + "districts_joined_raw.csv", "w") as fout:
    header = ['municipality_id', 'municipality', 'polling_station_id', 'polling_station_code', 'polling_station', 'senate_district_id', 'senate_district', 'momc_id', 'momc']
    dw = csv.DictWriter(fout, header)
    dw.writeheader()
    for municipality_id in data_obj:
        for polling_station_code in data_obj[municipality_id]:
            row = data_obj[municipality_id][polling_station_code]
            found = False
            if row['senate_district_id'] == '':
                for momc_id in momc_obj[row['municipality_id']]:
                    if momc_id in volby_obj.keys():
                        for polling_station_code in volby_obj[momc_id]:
                            if polling_station_code == row['polling_station_code']:
                                item = row
                                item['momc_id'] = momc_id
                                item['momc'] = momc_obj[row['municipality_id']][momc_id]['TEXT1']
                                item['senate_district_id'] = volby_obj[momc_id][polling_station_code]['senate_district_id']
                                dw.writerow(item)
                                # print(item)
                                found = True
            else:
                item = row
                item['momc_id'] = ''
                item['momc'] = ''
                dw.writerow(item)
                found = True
            if not found:
                print(row)
                item = row
                item['momc_id'] = ''
                item['momc'] = ''
                dw.writerow(item)


# manually:
# Praha	28247	9017	Volební okrsek 9017
# 503941	Libavá	21403	Volební okrsek 1
# 503941	Libavá	21405	Volební okrsek 2
# 503941	Libavá	21407	Volební okrsek 3
# 503941	Libavá	21409	Volební okrsek 4
# 503941	Libavá	21411	Volební okrsek 5
# 545422	Boletice	5443	Volební okrsek 1
# 555177	Hradiště	13887	Volební okrsek 1
# 555177	Hradiště	13889	Volební okrsek 2
# 555177	Hradiště	13891	Volební okrsek 3
# 592935	Březina	18173	Volební okrsek 1
# correct:
# 26387 -> 554782	Praha	26387	4132	Volební okrsek 4132	20		500119	Praha 4
# multi correct:
# from RUIAN (Praha) + senate results 26 / 2018
# 9004	104594	26
# 9005	28221	24
# 9006	28223	26
# 9007	28225	24
# 9008	28227	24
# 9009	28229	24
# 9010	28231	24
# 9011	28233	24
# 9012	28235	24
# 9013	28237	24
# 9014	28239	24
# 9015	28241	24
# 9016	28243	24
# 9017	28245	24
# 9018	28247	26
# 9019	28249	24
# 9020	28251	24
# 9021	28253	24
# 9022	28255	24
# 9023	28257	24
# 9024	28259	24
# 9025	28261	24
# 9026	28263	24
# 9027	28265	24
# 9028	28267	24
# 9029	28269	24
# 9030	28271	24
# 9031	28273	24
# 9032	28275	24
# 9033	28277	24
# 9034	28279	24
# 9035	28281	24
# 9036	28283	24
# 9037	28285	24
# 9038	28287	24
# 9039	28289	24
# 9040	28291	24
# 9041	28293	24
# 9042	28295	24
