"""Prepare list of senate districts."""

import csv
import datetime
import io
import requests

path = "/home/michal/dev/senatni-obvody/"

# list of senate districts
d = datetime.datetime.today().strftime('%d.%m.%Y')
url = "http://apl.czso.cz/iSMS/cisexp.jsp?kodcis=1057&typdat=0&cisvaz=43_304&datpohl=" + d + "&cisjaz=203&format=2&separator=%2C"
r = requests.get(url)
r.encoding = 'cp1250'
csvio = io.StringIO(r.text, newline="")
sds = []
for row in csv.DictReader(csvio):
    sds.append(row)

header = ['senate_district_id', 'senate_district_name', 'senate_district_category']
with open(path + "senate_districts.csv", "w") as fout:
    dw = csv.DictWriter(fout, header)
    dw.writeheader()
    for row in sds:
        item = {
            'senate_district_id': int(row['CHODNOTA']),
            'senate_district_name': row['SIDLO'],
            'senate_district_category': row['DELKA1OBD']
        }
        dw.writerow(item)
